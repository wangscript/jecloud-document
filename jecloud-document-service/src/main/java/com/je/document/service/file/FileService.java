/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service.file;

import com.je.common.base.DynaBean;
import com.je.common.base.document.FileSaveDTO;
import com.je.document.constants.DigestEnum;
import com.je.document.entity.DocumentFileDO;
import com.je.document.model.Bucket;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 文件通用业务处理
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/8/27
 */
public interface FileService {

    /**
     * 上传保存文件
     *
     * @param fileName    文件原始名称
     * @param contentType 文件类型
     * @param byteArray   文件byte数组
     * @param bucket      文件存储空间bucket
     * @return com.je.paas.document.model.dto.FileSaveDTO
     */
    default FileSaveDTO saveFile(String fileName, String contentType, byte[] byteArray, String bucket){
        return saveFile(fileName, contentType, byteArray, bucket);
    }

    /**
     * 上传保存文件
     *
     * @param fileName    文件原始名称
     * @param contentType 文件类型
     * @param byteArray   文件byte数组
     * @param bucket      文件存储空间bucket
     * @return com.je.paas.document.model.dto.FileSaveDTO
     */
    default FileSaveDTO saveFile(String fileName, String contentType, byte[] byteArray, Bucket bucket){
        return saveFile(fileName, contentType, byteArray, bucket, null);
    }

    /**
     * 上传保存文件
     *
     * @param fileName    文件原始名称
     * @param contentType 文件类型
     * @param byteArray   文件byte数组
     * @param bucket      文件存储空间bucket
     * @param dir         指定上传目录
     * @return com.je.paas.document.model.dto.FileSaveDTO
     */
    FileSaveDTO saveFile(String fileName, String contentType, byte[] byteArray, Bucket bucket, String dir);

    /**
     * 上传保存文件
     *
     * @param fileName    文件原始名称
     * @param contentType 文件类型
     * @param inputStream 文件输入流
     * @param bucket      文件存储空间bucket
     * @return com.je.paas.document.model.dto.FileSaveDTO
     */
    FileSaveDTO saveFile(String fileName, String contentType, InputStream inputStream, String bucket);

    /**
     * 获取文件摘要信息
     *
     * @param byteArray  文件内容
     * @param digestEnum 摘要算法类型
     * @return java.lang.String
     */
    String messageDigest(byte[] byteArray, DigestEnum digestEnum);

    /**
     * 获取文件缩略图
     *
     * @param byteArray   文件内容
     * @param contentType 文件类型
     * @param suffix      文件后缀
     * @return java.lang.String
     */
    InputStream thumbnail(String subfolder,byte[] byteArray, String contentType, String suffix, String basePath);

    /**
     * 读取文件
     *
     * @param filePath 文件标识路径
     * @param bucket   文件存储空间bucket
     * @return java.io.InputStream
     */
    InputStream readFile(String filePath, String bucket);

    /**
     * 构建zip
     * <p>
     * relNameList.size == filePathList.size == bucketList.size
     *
     * @param relNameList  文件名
     * @param filePathList 文件存储路径
     * @param bucketList   文件存储空间
     * @return java.io.InputStream
     */
    InputStream buildZip(List<String> relNameList, List<String> filePathList, List<String> bucketList);

    /**
     * 获取bucket业务对象
     *
     * @param bucket 文件存储空间bucket
     * @return com.je.paas.document.model.bean.Bucket
     */
    Bucket findBucket(String bucket);

    /**
     * 根据ID查找
     * @param id
     * @return
     */
    DynaBean findById(String id);

    /**
     * 按照ID删除
     * @param id
     */
    void deleteById(String id);

    /**
     * 文件是否存在
     *
     * @param filePath 文件标识路径
     * @param bucket   存储空间标识
     * @return java.lang.Boolean 存在返回true
     */
    Boolean existFile(String filePath, String bucket);

    DynaBean selectFileByDigestBean(String bucket, String digestType, String digestValue);

    /**
     * 查找文件通过摘要信息
     * @param digestCode 摘要类型
     * @param digestValue 摘要信息
     * @param bucket 存储桶信息
     * @return
     */
    DynaBean selectFileByDigestBucket(String digestCode, String digestValue, String bucket);
    DocumentFileDO selectFileByDigest(String bucket, String digestType, String digestValue);

    /**
     * 通过文件信息 删除服务器文件
     * @param fileDeleteInfos
     */
    void deleteFilesByFileInfo(List<Map<String, String>> fileDeleteInfos);
}
