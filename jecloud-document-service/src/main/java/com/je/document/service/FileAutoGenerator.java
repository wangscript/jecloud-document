/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service;

import com.je.document.model.Bucket;

/**
 * FileAutoGenerator
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/9/27
 */
public interface FileAutoGenerator {

    /**
     * 生成文件存储目录（例如：/aa/bb/cc）
     *
     * @param fileName    文件原始名称
     * @param contentType 文件类型
     * @param byteArray   文件byte数组
     * @param bucket      文件存储空间bucket
     * @return 文件存储路径
     */
    String pathGenerator(String fileName, String contentType, byte[] byteArray, Bucket bucket);

    /**
     * 生成文件存储名称（例如：xxx.zz）
     *
     * @param fileName    文件原始名称
     * @param contentType 文件类型
     * @param byteArray   文件byte数组
     * @param bucket      文件存储空间bucket
     * @return 存储文件名称
     */
    String fileNameGenerator(String fileName, String contentType, byte[] byteArray, Bucket bucket);

    /**
     * 是否校验唯一
     *
     * @return true|false
     */
    boolean checkDigest();

    /**
     * 是否生成缩略图
     *
     * @return true|false
     */
    default boolean thumbnailGenerator() {
        return true;
    }
}
