/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service;

import com.alibaba.fastjson2.JSONArray;
import com.je.common.base.DynaBean;
import com.je.common.base.document.FileCopyDTO;
import com.je.common.base.result.BaseRespResult;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.InputStream;
import java.util.List;

/**
 * 文档操作-业务拓展复杂Service
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/8/27
 */
public interface DocumentBusService extends DocumentService {

    /**
     * 下载
     *
     * @param request
     * @param fileKeyPath
     * @param fileKeyParam
     * @return
     */
    ResponseEntity<Part> previewAndDownload(HttpServletRequest request, String fileKeyPath, String fileKeyParam);

    /**
     * @param fileKeyPath
     * @param fileKeyParam
     * @return
     */
    BaseRespResult<String> previewTxt(String fileKeyPath, String fileKeyParam);

    /**
     * @param request
     * @param fileKeyPath
     * @param fileKeyParam
     * @return
     */
    ResponseEntity<InputStream> thumbnail(HttpServletRequest request, String fileKeyPath, String fileKeyParam);

    /**
     * 更新字段文件，逻辑删除无用附件
     *
     * @param userId      用户ID
     * @param tableCode   表名
     * @param pkValue     主键
     * @param fieldCode   字段名
     * @param usedFileKey 字段中包含的文件key
     */
    void updateFiledFile(String userId, String tableCode, String pkValue, String fieldCode, List<String> usedFileKey);

    /**
     * 复制指定业务数据的指定字段中的附件
     *
     * @param userId     用户ID
     * @param tableCode  业务表名
     * @param pkValue    业务主键
     * @param fieldCode  字段名称
     * @param newPkValue 新业务主键
     * @return java.util.List<com.je.paas.document.model.entity.FileCopyDTO> 复制出的新文件数据
     */
    default List<FileCopyDTO> copyFiles(String userId, String tableCode, String pkValue, String fieldCode, String newPkValue) {
        return copyFiles(userId, tableCode, pkValue, fieldCode, newPkValue, null, null, null);
    }

    /**
     * 复制指定业务数据的指定字段中的附件
     *
     * @param userId       操作用户ID
     * @param tableCode    业务表名
     * @param pkValue      业务主键
     * @param fieldCode    字段名称
     * @param newPkValue   新业务主键
     * @param newFuncCode  新功能CODE
     * @param newTableCode 新业务表名，为空则不变
     * @param newFiledCode 新字段名称，为空则不变
     * @return java.util.List<com.je.paas.document.model.entity.FileCopyDTO> 复制出的新文件数据
     */
    List<FileCopyDTO> copyFiles(String userId, String tableCode, String pkValue, String fieldCode, String newPkValue, String newFuncCode, String newTableCode, String newFiledCode);

    /**
     * 逻辑删除业务数据附件
     *
     * @param tableCode 业务表名
     * @param pkValues  主键集合,使用','分隔
     * @param userId    操作用户ID
     * @see #deleteFiles(String, String, String, String, String)
     */
    default void deleteFiles(String tableCode, String pkValues, String userId) {
        deleteFiles(tableCode, pkValues, null, null, userId);
    }

    /**
     * 逻辑删除业务数据附件
     *
     * @param tableCode  业务表名
     * @param pkValues   主键集合,使用','分隔
     * @param fieldCodes 指定要删除的字段; 多个使用','分隔
     * @param userId     操作用户ID
     * @see #deleteFiles(String, String, String, String, String)
     */
    default void deleteFiles(String tableCode, String pkValues, String fieldCodes, String userId) {
        deleteFiles(tableCode, pkValues, fieldCodes, null, userId);
    }

    /**
     * 逻辑删除业务数据附件
     *
     * @param tableCode   业务表名
     * @param pkValues    主键集合,使用','分隔
     * @param fieldCodes  指定要删除的字段; 多个使用','分隔
     * @param uploadTypes 指定要删除的上传类型; 多个使用','分隔
     * @param userId      操作用户ID
     */
    void deleteFiles(String tableCode, String pkValues, String fieldCodes, String uploadTypes, String userId);

    /**
     * 获取文件夹下文件元数据
     *
     * @param bucket 存储桶
     * @param path   文件夹路径
     */
    JSONArray listFilesMeta(String bucket, String path);

    /**
     * 检查文件后缀
     *
     * @param fileName
     * @return
     */
    void checkFileSuffix(String fileName);

    default String getPreviewOfficeUrl(HttpServletRequest request, String fileKey) {
        return getPreviewOfficeUrl(request, fileKey, null);
    }

    String getPreviewOfficeUrl(HttpServletRequest request, String fileKey, String previewType);

    JSONArray getFileInfo(String fileKeys);

    boolean existsOnDB(String fileKey);
}