/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.document.constants.SaveTypeEnum;
import com.je.document.exception.DocumentException;
import com.je.document.exception.DocumentExceptionEnum;
import com.je.document.model.Bucket;
import com.je.document.service.impl.FileOperateLocalServiceImpl;
import com.je.document.service.impl.FileOperateService;

import java.util.Date;
import java.util.Properties;

/**
 * 文件工具类工厂
 *
 * @author wangmm@ketr.com.cnx
 * @date 2019/8/29
 */
public class FileOperateFactory {

    /**
     * 获取文件操作工具类实例
     *
     * @param bucketBO 存储空间bucket业务对象
     * @return com.je.paas.document.util.file.file
     */
    public static FileOperateService getInstance(Bucket bucketBO) {
        SaveTypeEnum saveType = SaveTypeEnum.getDefault(bucketBO.getSaveType());
        if (SaveTypeEnum.aliyun == saveType) {
            return SpringContextHolder.getBean("aliyunFileOperater");
        } else if (SaveTypeEnum.local == saveType) {
            return SpringContextHolder.getBean("localFileOperater");
        } else if (SaveTypeEnum.tencent == saveType) {
            return SpringContextHolder.getBean("tencentFileOperater");
        } else {
            //拼接配置文件key
            throw new DocumentException("不支持" + saveType + "存储类型！", DocumentExceptionEnum.UNKOWN_ERROR);
        }
    }

    /**
     * 清理spring容器内注册的bean缓存
     *
     * @param bucketBO
     */
    public static void clearBeanCacheByStr(String bucketBO) {
        SaveTypeEnum saveType = SaveTypeEnum.getDefault(bucketBO);

         clearBean(saveType);
    }

    private static void clearBean(SaveTypeEnum saveType) {
        Config config = ConfigService.getConfig("oss");
        if (SaveTypeEnum.aliyun == saveType) {
            FileOperateService aliyunFileOperater = SpringContextHolder.getBean("aliyunFileOperater");
            Properties params = new Properties();
            params.put("oss.aliyun.accessBucket",config.getProperty("oss.aliyun.accessBucket",null));
            params.put("oss.aliyun.accessKey",config.getProperty("oss.aliyun.accessKey",null));
            params.put("oss.aliyun.secretKey",config.getProperty("oss.aliyun.secretKey",null));
            params.put("oss.aliyun.url",config.getProperty("oss.aliyun.url",null));
            params.put("oss.aliyun.permission",config.getProperty("oss.aliyun.permission",null));
            params.put("oss.aliyun.basePath",config.getProperty("oss.aliyun.basePath",null));

            aliyunFileOperater.reload(params);

        } else if (SaveTypeEnum.local == saveType) {

            FileOperateLocalServiceImpl localFileOperater = SpringContextHolder.getBean("localFileOperater");
            //从配置文件再次获取
            String basePath = config.getProperty("oss.local.basePath",null);
            //给这个bean再次赋值
            localFileOperater.setBasePath(basePath);
            localFileOperater.setModifiedTime(new Date());

        } else if (SaveTypeEnum.tencent == saveType) {
            FileOperateService tencentFileOperater = SpringContextHolder.getBean("tencentFileOperater");
            Properties properties = new Properties();
            properties.setProperty("",config.getProperty("oss.tencent.secretId",null));
            properties.setProperty("",config.getProperty("oss.tencent.secretKey",null));
            properties.setProperty("",config.getProperty("oss.tencent.bucketRegion",null));
            properties.setProperty("",config.getProperty("oss.tencent.bucketName",null));
            properties.setProperty("",config.getProperty("oss.tencent.permission",null));
            properties.setProperty("",config.getProperty("oss.tencent.url",null));
            properties.setProperty("",config.getProperty("oss.tencent.basePath",null));
            tencentFileOperater.reload(properties);
        } else {
            //拼接配置文件key
            throw new DocumentException("不支持" + saveType + "存储类型！", DocumentExceptionEnum.UNKOWN_ERROR);
        }
    }

}