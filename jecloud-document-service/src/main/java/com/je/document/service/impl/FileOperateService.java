/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service.impl;

import com.alibaba.fastjson2.JSONArray;
import com.je.common.base.document.UploadFileDTO;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

/**
 * 文件操作接口
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/8/27
 */
public interface FileOperateService {

    /**
     * 文件大小
     */
    String KEY_META_FILE_SIZE = "fileSize";
    /**
     * 文件名称
     */
    String KEY_META_FILE_NAME = "fileName";
    /**
     * 全路径
     */
    String KEY_META_FILE_FULL_PATH = "fullPath";
    /**
     * 文件后缀
     */
    String KEY_META_FILE_SUFFIX = "fileSuffix";
    /**
     * 文件相对路径
     */
    String KEY_META_FILE_RELATIVE_PATH = "relativePath";

    /**
     * 上传
     * @param subfolder
     * @param filePath 路径
     * @param file     文件流
     * @return com.je.paas.document.model.dto.UploadFileDTO
     */
    UploadFileDTO uploadFile(String subfolder,String filePath, InputStream file);

    String getBasePath();
    /**
     * 文件是否存在
     *
     * @param filePath 文件标识路径
     * @return java.lang.Boolean 存在返回true
     */
    Boolean existFile(String subfolder,String filePath);

    /**
     * 读取文件
     *
     * @param filePath 文件标识路径
     * @return java.io.InputStream
     */
    InputStream readFile(String subfolder,String filePath);

    /**
     * 获取临时url
     *
     * @param filePath   文件标识路径
     * @param expiration 有效时间
     * @return java.lang.String
     */
    String getUrl(String subfolder,String filePath, Date expiration);

    /**
     * 复制文件
     *
     * @param sourceFilePath 源文件 如:/A/B/file.txt
     * @param targetFilePath 需要复制的目标路径 如:/C/D/file.txt
     * @return boolean
     */
    boolean copy(String sourceFilePath, String targetFilePath);

    /**
     * 删除文件
     *
     * @param filePath 文件路径标识 如:/A/B/file.txt
     * @return boolean
     */
    boolean del(String subfolder,String filePath);

    /**
     * 列举文件夹下所有文件
     *
     * @param folder
     * @return
     */
    JSONArray listFilesMeta(String subfolder,String folder);

    /**
     * 是否最新bucket
     *
     * @param modifiedTime 最新更新时间
     * @return boolean
     */
    boolean isLastVersion(Date modifiedTime);

    /**
     * 重新加载
     * @param properties
     */
    default void reload(Properties properties){

    }

    default Properties getProperties(){
        return null;
    }

}