/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.exception;

/**
 * 文档模块异常信息
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/9/9
 */
public class DocumentException extends RuntimeException {

    private String code;
    private String errorCode;
    private String errorMsg;
    private Object[] errorParam;

    public DocumentException() {
        super();
    }

    public DocumentException(Throwable cause) {
        super(cause);
    }

    public DocumentException(DocumentExceptionEnum exceptionEnum) {
        super((exceptionEnum = DocumentExceptionEnum.getDefault(exceptionEnum)).toString());
        this.code = exceptionEnum.toString();
        this.errorCode = exceptionEnum.getCode();
        this.errorMsg = exceptionEnum.getMessage();
    }

    public DocumentException(DocumentExceptionEnum exceptionEnum, Throwable cause) {
        super((exceptionEnum = DocumentExceptionEnum.getDefault(exceptionEnum)).toString(), cause);
        this.code = exceptionEnum.toString();
        this.errorCode = exceptionEnum.getCode();
        this.errorMsg = exceptionEnum.getMessage();
    }

    public DocumentException(String message, DocumentExceptionEnum exceptionEnum) {
        super(message);
        exceptionEnum = DocumentExceptionEnum.getDefault(exceptionEnum);
        this.code = exceptionEnum.toString();
        this.errorCode = exceptionEnum.getCode();
        this.errorMsg = message;
    }

    public DocumentException(String message, DocumentExceptionEnum exceptionEnum, Throwable cause) {
        super(message, cause);
        exceptionEnum = DocumentExceptionEnum.getDefault(exceptionEnum);
        this.code = DocumentExceptionEnum.getDefault(exceptionEnum).toString();
        this.errorCode = DocumentExceptionEnum.getDefault(exceptionEnum).getCode();
        this.errorMsg = message;
    }

    public DocumentException(DocumentExceptionEnum exceptionEnum, Object[] params) {
        super((exceptionEnum = DocumentExceptionEnum.getDefault(exceptionEnum)).toString());
        this.code = DocumentExceptionEnum.getDefault(exceptionEnum).toString();
        this.errorCode = DocumentExceptionEnum.getDefault(exceptionEnum).getCode();
        this.errorMsg = exceptionEnum.getMessage();
        this.errorParam = params;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public Object[] getErrorParam() {
        return errorParam;
    }

    public String getCode() {
        return code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
