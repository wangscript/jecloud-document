/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.exception;

/**
 * 文档模块异常枚举(7700-7799)
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/9/9
 */
public enum DocumentExceptionEnum {

    /**
     * 查询一条记录错误
     */
    DOCUMENT_DAO_SELECT_ONE("7701", "查询一条记录错误"),

    /**
     * sql语句包含delete
     */
    DOCUMENT_DAO_EXECUTE_DELETE("7702", "sql语句包含delete"),

    /**
     * 业务元数据为空
     */
    DOCUMENT_SERVICE_METADATA_EMPTY("7721", "业务元数据为空"),

    /**
     * 业务元数据保存错误
     */
    DOCUMENT_SERVICE_METADATA_SAVE("7722", "业务元数据保存错误"),

    /**
     * 业务元数据保存错误
     */
    DOCUMENT_SERVICE_FILE_NOT_FIND("7723", "业务元数据保存错误"),

    /**
     * 修改文件引用
     */
    DOCUMENT_SERVICE_EDIT_FILE_FIND("7724", "修改文件引用"),

    /**
     * 保存业务逻辑失败
     */
    DOCUMENT_SERVICE_SAVE_FAIL("7725", "保存业务逻辑失败"),

    /**
     * 绑定业务逻辑失败
     */
    DOCUMENT_SERVICE_BOUND_FAIL("7725", "绑定业务逻辑失败"),

    /**
     * bucket找不到对应数据
     */
    DOCUMENT_MANAGER_BUCKET_NOT_FIND("7741", "bucket找不到"),

    /**
     * zip打包失败
     */
    DOCUMENT_MANAGER_ZIP_BUILd_ERROR("7742", "zip打包失败"),

    /**
     * 本地文件存储读取错误
     */
    DOCUMENT_MANAGER_LOCAL_FILE_ERROR("7743", "本地文件存储读取错误"),

    /**
     * 文件不存在
     */
    DOCUMENT_MANAGER_FILE_NOT_EXIST("7744", "文件不存在"),

    /**
     * 文件不存在
     */
    DOCUMENT_MANAGER_FILE_SAVE_FAIL("7745", "文件保存失败"),

    /**
     * 文件操作工具类初始化异常
     */
    DOCUMENT_FILE_OPERATE_INIT("7761", "文件操作工具类初始化异常"),

    /**
     * 文件读取错误
     */
    JE_DOC_FILE_READ_ERROR("7781", "文件读取错误"),

    /**
     * 绑定错误
     */
    JE_DOC_FILE_BOUND_ERROR("7782", "绑定错误"),

    /**
     * 权限过滤器异常
     */
    JE_RBAC_FILTER_ERROR("4999", "权限过滤器异常"),

    /**
     * 系统未知错误
     */
    UNKOWN_ERROR("9999", "系统未知错误"),

    /**
     * 未登录用户
     */
    UNKOWN_LOGINUSER("9000", "未登录用户"),

    /**
     * 未知文档错误
     */
    DOCUMENT_ERROR("7700", "未知文档错误");

    private String code;
    private String message;

    private DocumentExceptionEnum(String code) {
        this.code = code;
    }

    private DocumentExceptionEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static DocumentExceptionEnum getDefault(DocumentExceptionEnum exceptionEnum) {
        if (exceptionEnum == null) {
            return DOCUMENT_ERROR;
        }

        for (DocumentExceptionEnum value : DocumentExceptionEnum.values()) {
            if (value.equals(exceptionEnum)) {
                return exceptionEnum;
            }
        }

        return DOCUMENT_ERROR;
    }

    public static DocumentExceptionEnum getDefault(String code) {
        if (code == null || "".equals(code)) {
            return DOCUMENT_ERROR;
        }

        for (DocumentExceptionEnum value : DocumentExceptionEnum.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }

        return DOCUMENT_ERROR;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
