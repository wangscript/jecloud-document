/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.util;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.google.common.base.Strings;
import com.je.document.constants.OrderTypeEnum;

public class SqlUtil {

    /**
     * json order格式转换为正常order
     * [{"code":"time","type":"desc"}] 转换为 time desc;
     * @param jsonOrder
     * @return
     */
    public static String convertDiskNodeJsonOrder(String jsonOrder){
        if(Strings.isNullOrEmpty(jsonOrder)){
            return null;
        }
        JSONArray orderArray = JSON.parseArray(jsonOrder);
        StringBuilder sb = new StringBuilder();
        sb.append("node_type asc");
        String code;
        for (int i = 0; i < orderArray.size(); i++) {
            sb.append(",");
            code = orderArray.getJSONObject(i).getString("code");
            if (OrderTypeEnum.TIME.getCode().equals(code)) {
                sb.append("modified_time");
                //根据名称
            } else if (OrderTypeEnum.NAME.getCode().equals(code)) {
                sb.append("node_name");
                //根据文件大小
            } else if (OrderTypeEnum.SIZE.getCode().equals(code)) {
                sb.append("file_size");
            }
            sb.append(" ");
            sb.append(orderArray.getJSONObject(i).getString("type"));
            if(orderArray.size() < i+1){
                sb.append(",");
            }
        }
        return sb.toString();
    }

}
